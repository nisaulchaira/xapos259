package com.xapos259.security.jwt;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.xapos259.security.service.UserDetailsImpl;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtUtils {

	private static final Logger Logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${com.app.jwtSecret}")
	private String jwtSecret;

	@Value("${com.app.jwtExpiration}")
	private int jwtExpirationMs;

	public String generateJwtToken(Authentication authentication) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

		return Jwts.builder().setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	public String getUsernameFromJwt(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException err) {
			Logger.error("Invalid Jwt Signature : {}", err.getMessage());
		} catch (MalformedJwtException err) {
			Logger.error("Invalid Jwt Token : {}", err.getMessage());
		} catch (ExpiredJwtException err) {
			Logger.error("JWT Token is expirated : {}", err.getMessage());
		} catch (UnsupportedJwtException err) {
			Logger.error("JWT Token is unsupported : {}", err.getMessage());
		} catch (IllegalArgumentException err) {
			Logger.error("JWT claims string is empty : {}", err.getMessage());
		}
		return false;
	}
}
